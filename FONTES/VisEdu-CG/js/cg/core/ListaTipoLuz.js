/**
 * 
 */

ListaTipoLuz = {
	'Ambient'     : 0,		
	'Hemisphere'  : 1,
	'Directional' : 2,
	'PointLight'  : 3,
	'SpotLight'   : 4		
};