/**
 * 
 */

ListaPrimitivas = {
	'Vertices'   : 'GL_POINTS',
	'Aberto'     : 'GL_LINE_STRIP',
	'Fechado'    : 'GL_LINE_LOOP',
	'Preenchido' : 'GL_POLYGON'
};