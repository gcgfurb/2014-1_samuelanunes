/**
 * 
 */

PainelPrincipal = function () {

	UI.Panel.call(this);	
	
	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 1, 9999);
	
	var sceneHelper = new THREE.Scene();
	scene.add(sceneHelper);
	
	camera.position.x = 0;
	camera.position.y = 700;
	camera.position.z = 1800;
	camera.up.x = 0;
	camera.up.y = 1;
	camera.up.z = 0;		
	camera.lookAt( new THREE.Vector3().set(0, 0, 0) );	
	
	//SELECAO
	var mouse3D = {x: 0, y: 0};
	var intersectableObjectsList = [];	
	var OBJETO_SELECIONADO;	
	
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
		
	renderer.domElement.ondblclick = selecionaObjeto;
	renderer.domElement.backgroundcolor = 0x555555;
	
	document.body.appendChild(renderer.domElement);
	
	//CONTROLER
	var controls = new THREE.TrackballControls( camera, renderer.domElement  );
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;
	controls.noRotate = false;
	controls.noZoom = false;
	controls.noPan = false;
	controls.staticMoving = false;
	controls.dynamicDampingFactor = 0.3;
	
	this.renderizar = function() {		
		controls.update();
		renderer.render(scene, camera);
	};
	
	this.criarObjGrafico = function () {
		AddLuz();
		
		//desenharCubo();		
		desenharPoligono();		
		desenharPoligonoPreenchido();
		
		desenharGrade();		
		desenharEixos();	
	};
	
	function AddLuz() {
		AddHemisphereLight();
		//AddDirectionalLight();
		//AddPointLight();
		//AddSpotLight();
		//AddAmbientLight();
	};
	
	function AddHemisphereLight() {
		var skyColor = 0xFFFFFF;
		var groundColor = 0xFFFFFF;
		var intensity = 1;
		var light = new THREE.HemisphereLight( skyColor, groundColor, intensity );
		scene.add(light);
	}
	
	function AddDirectionalLight() {
		var color = 0xffffff;
		var intensity = 1;

		var light = new THREE.DirectionalLight( color, intensity );		
		light.position.set(300, 600, 0);
		
		light.target.position.set(300, 0, 0);
		light.target.matrixWorld.elements[ 12 ] = light.target.position.x;
		light.target.matrixWorld.elements[ 13 ] = light.target.position.y;
		light.target.matrixWorld.elements[ 14 ] = light.target.position.z;
				
		scene.add( light );
		
		var helper = new THREE.DirectionalLightHelper( light, 20 );
		scene.add( helper );
		helper.update();
	}
	
	function AddPointLight() {		
		var color = 0xffffff;
		var intensity = 1;
		var distance = 0;

		var light = new THREE.PointLight( color, intensity, distance );
		light.position.set( 150, 150, 150 );
		scene.add( light );
		var helper = new THREE.PointLightHelper( light, 20 );
		scene.add( helper );
	}
	
	function AddSpotLight() {
		var color = 0xffffff;
		var intensity = 1;
		var distance = 0;
		var angle = 0.10;
		var exponent = 10;

		var light = new THREE.SpotLight( color, intensity, distance, angle, exponent );
		light.position.set(0, 400, 0 );
		
		light.target.position.set(300, 100, 0);
		
		scene.add( light );
		
		var helper = new THREE.SpotLightHelper( light, 10 );
		scene.add( helper );
	}
	
	function AddAmbientLight() {
		var color = 0x404040;

		var light = new THREE.AmbientLight( color );
		light.position.set(600, 600, 600 );
		scene.add( light );
	}
	
	function desenharCubo() {
		var geometry = new THREE.CubeGeometry(200, 200, 200);
		var material = new THREE.MeshPhongMaterial({color: 0x00ff00});
		var cube = new THREE.Mesh(geometry, material);
		cube.position.set(300, 100, 0);
		cube.nome = 'Cubo';
		scene.add(cube);
		intersectableObjectsList.push(cube);
	}
	
	function desenharPoligono() {
		var points = [];
		
		points.push(new THREE.Vector3(-100,  100, 0));
		points.push(new THREE.Vector3(100,  -100, 0));
		points.push(new THREE.Vector3(18, 245, 0));
		points.push(new THREE.Vector3(25, 545, 0));
		//points.push(new THREE.Vector3(-100.0,  -150.0, 100.0));
		
		var geometry = new THREE.Geometry();
		geometry.vertices = points;
		geometry.computeLineDistances();
		geometry.computeBoundingSphere();
								
		var material = new THREE.LineBasicMaterial( { linewidth: 2, color: 0xBE2EF2, transparent: false } );
		var line = new THREE.Line(geometry, material, THREE.LineStrip);
		line.position.set(260, 200, 0);
		line.nome = 'Poligono';
		scene.add(line);
		intersectableObjectsList.push(line);
		
		var squareGeometry = new THREE.Geometry();
		squareGeometry.vertices.push(new THREE.Vector3(-100,  100, 0));
		squareGeometry.vertices.push(new THREE.Vector3(100,  -100, 0));
		squareGeometry.vertices.push(new THREE.Vector3(18, 245, 0));
		squareGeometry.vertices.push(new THREE.Vector3(25, 545, 0));			
		
		var v1 = 1; 
		var v2 = 2;
		
		for (var i = 0; i < (4 - 2); i++) {							
			squareGeometry.faces.push(new THREE.Face3(0, v1, v2));
			v1++;
			v2++;
		}		

		// Create a white basic material and activate the 'doubleSided' attribute.
		var squareMaterial = new THREE.MeshPhongMaterial({
		 color:0xfff357,
		 opacity: 0.01,
		 transparent: true,
		 side:THREE.DoubleSide		 
		});

		// Create a mesh and insert the geometry and the material. Translate the whole mesh
		// by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
		var squareMesh = new THREE.Mesh(squareGeometry, squareMaterial);		
		squareMesh.position.set(260, 200, 0);
		scene.add(squareMesh);
		squareMesh.nome = 'Poligono';
		intersectableObjectsList.push(squareMesh);
	}
	
	function desenharPoligonoPreenchido() {
		var squareGeometry = new THREE.Geometry();
		squareGeometry.vertices.push(new THREE.Vector3(-100,  100, 0));
		squareGeometry.vertices.push(new THREE.Vector3(100,  -100, 0));
		squareGeometry.vertices.push(new THREE.Vector3(18, 245, 0));
		squareGeometry.vertices.push(new THREE.Vector3(25, 545, 0));
		
//		squareGeometry.faces.push(new THREE.Face3(0, 1, 3));
//		squareGeometry.faces.push(new THREE.Face3(1, 2, 3));			
		
		var v1 = 0; 
		var v2 = 1;
		
		for (var i = 0; i < (4 - 2); i++) {							
			squareGeometry.faces.push(new THREE.Face3(v1, v2, 3));
			v1++;
			v2++;
		}		

		// Create a white basic material and activate the 'doubleSided' attribute.
		var squareMaterial = new THREE.MeshPhongMaterial({
		 color:0xfff357,
		 side:THREE.DoubleSide		 
		});

		// Create a mesh and insert the geometry and the material. Translate the whole mesh
		// by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
		var squareMesh = new THREE.Mesh(squareGeometry, squareMaterial);		
		squareMesh.position.set(260, 200, 0);
		scene.add(squareMesh);
		squareMesh.nome = 'Poligono preenchido';
		intersectableObjectsList.push(squareMesh);
	}
	
	function desenharGrade() {
		var grid = new THREE.GridHelper( 500, 25 );
		sceneHelper.add( grid );
	}
	
	function desenharEixos() {
		var geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 700, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.computeLineDistances();
		var material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0xFF0000, transparent: true } );
		var lineX = new THREE.Line( geometry, material );
		sceneHelper.add(lineX);
		
		geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 700, 0 ) );
		geometry.computeLineDistances();
		material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0x00FF00, transparent: true } );
		var lineY = new THREE.Line( geometry, material );			
		sceneHelper.add( lineY );	
		
		geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 700 ) );
		geometry.computeLineDistances();
		material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0x0000FF, transparent: true } );
		var lineZ = new THREE.Line( geometry, material );
		sceneHelper.add( lineZ );
	}
	
	function selecionaObjeto( event ) {
		mouse3D.x = ( ( event.clientX) / (renderer.domElement.width) )  * 2 - 1;
		mouse3D.y = - ( ( event.clientY) / renderer.domElement.height ) * 2 + 1;
		
		var vetor = new THREE.Vector3(mouse3D.x, mouse3D.y, 1);
		//Cria o projetor3D da cena3D
		var projetor3D = new THREE.Projector(); 
		projetor3D.unprojectVector(vetor, camera);
		
		var raio3D = new THREE.Raycaster();
		raio3D.set( camera.position, vetor.sub(camera.position).normalize());
		
		// cria um array com todos os objetos da cena3D que sao interseccionados pelo raio.
		var intersects = raio3D.intersectObjects(intersectableObjectsList);				
		
		if (intersects.length > 0 ) {
			alert('Objeto selecionado: ' + intersects[0].object.nome);
		}
	}
};

PainelPrincipal.prototype = Object.create(UI.Panel.prototype);