/**
 * 
 */

PainelPrincipal = function () {

	UI.Panel.call(this);
	
	var range_1_Min = -200, 
		range_1_Max = 200, 
		range_2_Min = -200, 
		range_2_Max = 200;
	var range_1, range_2;
	var zValor = 800;
	
	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 1, 9999);
	
	var sceneHelper = new THREE.Scene();
	scene.add(sceneHelper);
	
	camera.position.x = 0;
	camera.position.y = 600;
	camera.position.z = 1800;
	camera.up.x = 0;
	camera.up.y = 1;
	camera.up.z = 0;		
	camera.lookAt( new THREE.Vector3().set(0, 0, 0) );	
	
	//SELECAO
	var mouse3D = {x: 0, y: 0};
	var intersectableObjectsList = [];	
	var OBJETO_SELECIONADO;	
	
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
		
	renderer.domElement.ondblclick = selecionaObjeto;
	renderer.domElement.backgroundcolor = 0x555555;
	
	document.body.appendChild(renderer.domElement);
	
	//CONTROLER
	var controls = new THREE.TrackballControls( camera, renderer.domElement  );
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;
	controls.noRotate = false;
	controls.noZoom = false;
	controls.noPan = false;
	controls.staticMoving = false;
	controls.dynamicDampingFactor = 0.3;
	
	this.renderizar = function () {		
		controls.update();
		renderer.render(scene, camera);
	};
	
	this.criarObjGrafico = function () {
		desenharCubo();		
		desenharTriangulo();		
		desenharPoligono();
		
		desenharPoligono3();
		desenharPoligono4();
		
		desenharGrade();		
		desenharEixos();		
	};
	
	function desenharCubo() {
		var geometry = new THREE.CubeGeometry(100, 100, 100);
		var material = new THREE.MeshBasicMaterial({color: 0x00ff00});
		var cube = new THREE.Mesh(geometry, material);
		cube.position.set(-400, 400, 0);
		cube.nome = 'Cubo';
		scene.add(cube);
		intersectableObjectsList.push(cube);
	}
	
	function desenharTriangulo() {
		var lineGeom = new THREE.Geometry();
		lineGeom.vertices.push( new THREE.Vector3(-150,-150,0) );
		lineGeom.vertices.push( new THREE.Vector3(150,-150,0) );
		lineGeom.vertices.push( new THREE.Vector3(0, 150,0) );
		lineGeom.vertices.push( new THREE.Vector3(-150,-150,0) );
		lineGeom.colors.push( new THREE.Color(0xff0000) );
		lineGeom.colors.push( new THREE.Color(0x00ff00) );
		lineGeom.colors.push( new THREE.Color(0x0000ff) );
		lineGeom.colors.push( new THREE.Color(0xff0000) );
		
		var lineMat = new THREE.LineBasicMaterial({
		    linewidth: 3,
		    vertexColors: true
		});
		
		var triangle = new THREE.Line( lineGeom, lineMat );
		triangle.nome = 'Triangulo';
		triangle.position.set(0, 500, 0);
		scene.add(triangle);  // scene is of type THREE.Scene
		intersectableObjectsList.push(triangle);
	}
	
	function desenharPoligono() {
		var points = [];
		
		points.push(new THREE.Vector3(-100.0,  -150.0, 100.0));
		points.push(new THREE.Vector3( -250.0,  200.0, -100.0));
		points.push(new THREE.Vector3( 175.0, 140.0, -50.0));
		points.push(new THREE.Vector3( 250.0, 25.0, 100.0));
		points.push(new THREE.Vector3(-100.0,  -150.0, 100.0));
		
		var geometry = new THREE.Geometry();
		geometry.vertices = points;
		geometry.computeLineDistances();
		geometry.computeBoundingSphere();
								
		var material = new THREE.LineBasicMaterial( { linewidth: 2, color: 0xBE2EF2, transparent: false } );
		var line = new THREE.Line(geometry, material, THREE.LineStrip);
		line.position.set(0, 0, 0);
		line.nome = 'Poligono Linha';
		scene.add(line);
		intersectableObjectsList.push(line);
	}
	
	function desenharPoligono3() {
		var squareGeometry = new THREE.Geometry();
		squareGeometry.vertices.push(new THREE.Vector3(-100.0,  -150.0, 100.0));
		squareGeometry.vertices.push(new THREE.Vector3( -250.0,  200.0, -100.0));
		squareGeometry.vertices.push(new THREE.Vector3( 175.0, 140.0, -50.0));
		squareGeometry.vertices.push(new THREE.Vector3( 250.0, 25.0, 100.0));
		
		
		var v1 = 1; 
		var v2 = 2;
		
		for (var i = 0; i < (4 - 2); i++) {							
			squareGeometry.faces.push(new THREE.Face3(0, v1, v2));
			v1++;
			v2++;
		}
		

		// Create a white basic material and activate the 'doubleSided' attribute.
		var squareMaterial = new THREE.MeshBasicMaterial({
		 color:0xfff357,
		 opacity: 0.01,
		 transparent: true,
		 side:THREE.DoubleSide		 
		});

		// Create a mesh and insert the geometry and the material. Translate the whole mesh
		// by 1.5 on the x axis and by 4 on the z axis and add the mesh to the scene.
		var squareMesh = new THREE.Mesh(squareGeometry, squareMaterial);		
		squareMesh.position.set(0, 0, 0);
		scene.add(squareMesh);
		squareMesh.nome = 'Poligono';
		intersectableObjectsList.push(squareMesh);
	}
	
	function desenharPoligono4() {
		var points = [];
		points.push(new THREE.Vector3(-100.0,  -150.0, 100.0));
		points.push(new THREE.Vector3( -250.0,  200.0, -100.0));
		points.push(new THREE.Vector3( 175.0, 140.0, -50.0));
		points.push(new THREE.Vector3( 250.0, 25.0, 100.0));
		points.push(new THREE.Vector3(-100.0,  -150.0, 100.0));			
		
		var squareShape = new THREE.Shape( points );
		var geometry = new THREE.ShapeGeometry( squareShape );		
		var material = new THREE.MeshBasicMaterial( { color: 0x357fff } );								
		var poligon = new THREE.Mesh( geometry, material );
		
		poligon.position.set(-500, 0, 0);
		poligon.nome = 'Poligono Shape';
		scene.add(poligon);
		intersectableObjectsList.push(poligon);
	}	
	
	function desenharGrade() {
		var grid = new THREE.GridHelper( 500, 25 );
		sceneHelper.add( grid );
	}
	
	function desenharEixos() {
		var geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 700, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.computeLineDistances();
		var material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0xFF0000, transparent: true } );
		var lineX = new THREE.Line( geometry, material );
		sceneHelper.add(lineX);
		
		geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 700, 0 ) );
		geometry.computeLineDistances();
		material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0x00FF00, transparent: true } );
		var lineY = new THREE.Line( geometry, material );			
		sceneHelper.add( lineY );	
		
		geometry = new THREE.Geometry();
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 0 ) );
		geometry.vertices.push( new THREE.Vector3 ( 0, 0, 700 ) );
		geometry.computeLineDistances();
		material = new THREE.LineBasicMaterial( { linewidth: 1, color: 0x0000FF, transparent: true } );
		var lineZ = new THREE.Line( geometry, material );
		sceneHelper.add( lineZ );
	}
	
	function selecionaObjeto( event ) {
		mouse3D.x = ( ( event.clientX) / (renderer.domElement.width) )  * 2 - 1;
		mouse3D.y = - ( ( event.clientY) / renderer.domElement.height ) * 2 + 1;
		
		var vetor = new THREE.Vector3(mouse3D.x, mouse3D.y, 1);
		//Cria o projetor3D da cena3D
		var projetor3D = new THREE.Projector(); 
		projetor3D.unprojectVector(vetor, camera);
		
		var raio3D = new THREE.Raycaster();
		raio3D.set( camera.position, vetor.sub(camera.position).normalize());
		
		// cria um array com todos os objetos da cena3D que sao interseccionados pelo raio.
		var intersects = raio3D.intersectObjects(intersectableObjectsList);				
		
		if (intersects.length > 0 ) {
			alert('Objeto selecionado: ' + intersects[0].object.nome);
		}
	}
};

PainelPrincipal.prototype = Object.create(UI.Panel.prototype);